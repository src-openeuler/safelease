Name:       safelease
Version:    1.0.1
Release:    2
Summary:    Legacy locking utility for VDSM

License:    GPLv2+
URL:        https://www.ovirt.org/develop/developer-guide/vdsm/safelease.html
#from  http://resources.ovirt.org/pub/src/%{name}/%{name}-%{version}.tar.gz
Source0:    %{name}-%{version}.tar.gz
BuildRequires:  gcc
BuildRequires: autoconf
BuildRequires: automake

%description
Safelease is a legacy cluster lock utility used by VDSM. It is based on
the algorithm presented in the article "Light-Weight Leases for
Storage-Centric Coordination" by G Chockler and D Malkhi.

%prep
%autosetup -n %{name}-%{version}

%build
./autogen.sh --system

%configure
make %{?_smp_mflags}

%install
%make_install

%files
%doc AUTHORS README
%{!?_licensedir:%global license %%doc}
%license COPYING
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/%{name}

%changelog
* Wed Jul 7 2021 kymayl<mayilin@kylinos.cn> - 1.0.1-2
- upgrade package
* Sun May 23 2021 huanghaitao <huanghaitao8@huawei.com> - 1.0-9
- package init